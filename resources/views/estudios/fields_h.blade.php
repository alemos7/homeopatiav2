<!-- Tipo Field -->
@if(!isset($estudios))
    <div class="row">
        <div class="form-group col-sm-12">
            <label for=""><b>{{ _i('Tipo') }}:</b> {{ _i('Humano') }}</label>
            <input style="display: none" type="text"  name="tipo"  id="tipo" value="97">
{{--            <select class="form-control" name="tipo" id="tipo" required--}}
{{--                    title="{{_i('Nombre/s:Los datos deben ser cargados en la planilla de consulta sin acentos o simbolos. Debe ingresarse el texto con las letras puras correspondientes (no agregar letras con ´, ¨, ^, \', `, ~, etc.).')}}">--}}
{{--                <option value="97" @if(@old("tipo")==97) selected @endif>{{ _i('Humano') }}</option>--}}
{{--                <option value="12" @if(@old("tipo")==12) selected @endif>{{ _i('Animal') }}</option>--}}
{{--            </select>--}}
            {!! $errors->first('tipo', '<span class="invalid-feedback"><strong>:message</strong></span>') !!}
        </div>
    </div>
@endif

<div id="EHumano">
    <div class="row">
        <!-- H Nombre Field -->
        <div class="form-group col-sm-4">
            <label for=""><b>{{ _i('Nombre/s') }}:</b></label>
            {!! Form::text('h_nombre', null, ['class' => 'form-control'. ( $errors->has('h_nombre') ? ' is-invalid' : '' ), "title"=>_i("Nombre/s:Los datos deben ser cargados en la planilla de consulta sin acentos o simbolos. Debe ingresarse el texto con las letras puras correspondientes (no agregar letras con ´, ¨, ^, \', `, ~, etc.).")]) !!}
            {!! $errors->first('h_nombre', '<span class="invalid-feedback"><strong>:message</strong></span>') !!}
        </div>

        <!-- H Apellido Field -->
        <div class="form-group col-sm-4">
            <label for=""><b>{{ _i('Apellido/s') }}:</b></label>
            {!! Form::text('h_apellido', null, ["title"=>_i("Apellido/s::Los datos deben ser cargados en la planilla de consulta sin acentos o simbolos. Debe ingresarse el texto con las letras puras correspondientes (no agregar letras con ´, ¨, ^, \', `, ~, etc.)."), 'class' => 'form-control'. ( $errors->has('h_apellido') ? ' is-invalid' : '' )]) !!}
            {!! $errors->first('h_apellido', '<span class="invalid-feedback"><strong>:message</strong></span>') !!}
        </div>

        <!-- H Identifica Field -->
        <div class="form-group col-sm-4">
            <label for=""><b>{{ _i('Nombre por el cual se reconoce') }}:</b></label>
            {!! Form::text('h_identifica', null, ["title"=>_i("Apodo::en algunas ocasiones la persona se identifica, no con alguno de sus nombres propios, sino con un &quot;apodo&quot; que reemplaza en importancia a su propio nombre."),'class' => 'form-control'. ( $errors->has('h_identifica') ? ' is-invalid' : '' )]) !!}
            {!! $errors->first('h_identifica', '<span class="invalid-feedback"><strong>:message</strong></span>') !!}
        </div>
    </div>

    <div class="row">

        <!-- H Iniciales Field -->
        <div class="form-group col-sm-2">
            <label for=""><b>{{ _i('Iniciales') }}:</b></label>
            {!! Form::text('h_iniciales', null, ["title"=>_i("Iniciales::Los datos deben ser cargados en la planilla de consulta sin acentos o simbolos. Debe ingresarse el texto con las letras puras correspondientes (no agregar letras con ´, ¨, ^, \', `, ~, etc.)"),'class' => 'form-control'. ( $errors->has('h_iniciales') ? ' is-invalid' : '' )]) !!}
            {!! $errors->first('h_iniciales', '<span class="invalid-feedback"><strong>:message</strong></span>') !!}
        </div>
    @if(!isset($estudios))
        <!-- H Pais Field -->
        <!-- Fecha Field -->
{{--        <div class="form-group col-sm-4">--}}
{{--            <label for="fecha_humano"><b>{{ _i('Fecha') }}:</b></label>--}}
{{--            <input class="form-control {{$errors->has('fecha_humano') ? ' is-invalid' : ''}}" id="fecha_humano" name="fecha_humano" type="date" value="{{($fecha)? $fecha : ''}}"--}}
{{--            {!! $errors->first('fecha_humano', '<span class="invalid-feedback"><strong>:message</strong></span>') !!}--}}
{{--        </div>--}}
            <?php
            if(app()->getLocale() == "es") {
            ?>
                <div class="form-group col-sm-6">
                    <div style="margin-bottom: -25px;"  class="row">
                        <div class="form-group col-sm-12">
                            <label for="fecha_humano"><b>{{ _i('Fecha de Nacimiento') }}:</b></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-4">
                            <select class="form-control" name="h_dia" id="h_dia" required="" title="">
                                <option>{{ _i('Dia') }}</option>
                                <?php
                                if (!isset($dia)) $dia = null;
                                for ($i = 1; $i <= 31; $i++) {
                                ?>
                                <option <?php if($i == $dia ) { ?> selected <?php } ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-sm-4">
                            <select class="form-control" name="h_mes" id="h_mes" required="" title="">
                                <option selected>{{ _i('Mes') }}</option>
                                <?php
                                if (!isset($mes)) $mes = null;
                                for ($i = 1; $i <= 12; $i++) {
                                ?>
                                <option <?php if($i == $dia ) { ?> selected <?php } ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-sm-4">
                            <select class="form-control" name="h_anio" id="h_anio" required="" title="">
                                <option selected>{{ _i('Año') }}</option>
                                <?php
                                if (!isset($anio)) $anio = null;
                                for ($i = date('Y'); $i >= 1920; $i--) {
                                ?>
                                <option <?php if($i == $anio ) { ?> selected <?php } ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>

            <?php
            }else{
                ?>
                <div class="form-group col-sm-6">
                    <div style="margin-bottom: -25px;"  class="row">
                        <div class="form-group col-sm-12">
                            <label for="fecha_humano"><b>{{ _i('Fecha de Nacimiento') }}:</b></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-4">
                            <select class="form-control" name="h_anio" id="h_anio" required="" title="">
                                <option>{{ _i('Año') }}</option>
                                <?php
                                if (!isset($anio)) $anio = null;
                                for ($i = date('Y'); $i >= 1920; $i--) {
                                ?>
                                <option <?php if($i == $anio ) { ?> selected <?php } ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-sm-4">
                            <select class="form-control" name="h_mes" id="h_mes" required="" title="">
                                <option>{{ _i('Mes') }}</option>
                                <?php
                                if (!isset($mes)) $mes = null;
                                for ($i = 1; $i <= 12; $i++) {
                                ?>
                                <option <?php if($i == $mes ) { ?> selected <?php } ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-sm-4">
                            <select class="form-control" name="h_dia" id="h_dia" required="" title="">
                                <option>{{ _i('Dia') }}</option>
                                <?php
                                if (!isset($dia)) $dia = null;
                                for ($i = 1; $i <= 31; $i++) {
                                    ?>
                                    <option <?php if($i == $dia ) { ?> selected <?php } ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>

        <div class="form-group col-sm-4">
            <div class="form-group">
                <label for=""><b>{{ _i('País') }}:</b></label>
                <select class="form-control {{$errors->has('pais_id') ? ' is-invalid' : ''}}" name="pais_id" id="pais_id">
                    <option value="">{{ _i(':: Seleccione un País ::') }}</option>
                    <?php foreach ($paises as $pais) { ?>
                    <option value="{{$pais->id}}" @isset($estudios) @if($pais->id==$estudios->pais_id) selected="selected" @endif @endisset @if(@old("pais_id")==$pais->id) selected="selected" @endif>{{$pais->name}}</option>
                    <?php } ?>
                </select>
                {!! $errors->first('pais_id', '<span class="invalid-feedback"><strong>:message</strong></span>') !!}
            </div>
        </div>
    @endif
    </div>
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    <input type="hidden" id="ip" name="ip" value="127.0.0.1">
    <input type="hidden" id="user_agent" name="user_agent" value="Firefox">

    <button type="submit" class="btn btn-success" onclick="return confirm('{{_i('¿Esta Realmente seguro de crear este Estudio con estos Datos?')}}')">{{_i('Realizar Estudio')}}</button>
    <a href="{!! route('estudios.index') !!}" class="btn btn-success">{{ _i('Cancelar') }}</a>
</div>

@section('scripts')
    <script>
        var isSafari = window.safari !== undefined;
        if (isSafari) {
            //document.getElementById('fecha_humano').type = 'text';
            //document.getElementById('fecha_animal').type = 'text';
            if ( $('[type="date"]').prop('type') != 'date' ) {
                $('[type="date"]').datepicker();
            }
        }


        $(document).ready(function () {



            let estudioTipo = '{!! (isset($estudios->tipo))? $estudios->tipo : 0 !!}';

            if(estudioTipo!=0){
                if (estudioTipo == 'humano') {
                    $('#EHumano').css('display', 'block');
                    $('#EAnimal').css('display', 'none');
                } else if (estudioTipo == 'animal') {
                    $('#EHumano').css('display', 'none');
                    $('#EAnimal').css('display', 'block');
                } else {
                    $('#EHumano').css('display', 'none');
                    $('#EAnimal').css('display', 'none');
                }
            }else{
                if ($('#tipo').val() == '97') {
                    $('#EHumano').css('display', 'block');
                    $('#EAnimal').css('display', 'none');
                } else if ($('#tipo').val() == '12') {
                    $('#EHumano').css('display', 'none');
                    $('#EAnimal').css('display', 'block');
                } else {
                    $('#EHumano').css('display', 'none');
                    $('#EAnimal').css('display', 'none');
                }
            }

            $('#tipo').change(function () {

                if ($('#tipo').val() == '97') {
                    $('#EHumano').css('display', 'block');
                    $('#EAnimal').css('display', 'none');
                } else if ($('#tipo').val() == '12') {

                    $('#EHumano').css('display', 'none');
                    $('#EAnimal').css('display', 'block');
                    document.getElementById("EAnimal").style.display = "block";
                } else {
                    $('#EHumano').css('display', 'none');
                    $('#EAnimal').css('display', 'none');
                }
            });
        });
    </script>
@endsection
